// git is very cringe sometimes

import { encode, decode } from "https://deno.land/std/encoding/base64.ts";

let reps;

const username =
  Deno.env.get("GITEA_USERNAME") ||
  (() => {
    throw new Error("GITEA_USERNAME required");
  })();
const password =
  Deno.env.get("GITEA_PASSWORD") ||
  (() => {
    throw new Error("GITEA_PASSWORD required");
  })();
const instance =
  Deno.env.get("GITEA_INSTANCE") ||
  (() => {
    throw new Error("GITEA_INSTANCE required");
  })();

console.log("0. Instance Healthcheck...");
try {
  await fetch(instance);
} catch (e) {
  console.error(e);
  throw new Error("Healthcheck failed");
}

console.log("1. Getting all repos...");
try {
  const repos = await (
    await fetch(`${instance}/api/v1/user/repos?limit=100`, {
      method: "GET",
      headers: {
        Authorization: "Basic " + encode(`${username}:${password}`),
      },
    })
  ).json();
  reps = repos.map(({ name }) => {
    console.log(`- ${username}/${name}`);
    return name;
  });
  console.log(`Found ${reps.length} repos.`);
} catch (e) {
  console.error(e);
  throw new Error("Getting repos failed");
}

console.log("2. Getting all repos infos...");
try {
  const tempreps = [];
  for (const name of reps) {
    const repo = await (
      await fetch(`${instance}/api/v1/repos/${username}/${name}`, {
        method: "GET",
        headers: {
          Authorization: "Basic " + encode(`${username}:${password}`),
        },
      })
    ).json();
    const { topics } = await (
      await fetch(`${instance}/api/v1/repos/${username}/${name}/topics`, {
        method: "GET",
        headers: {
          Authorization: "Basic " + encode(`${username}:${password}`),
        },
      })
    ).json();
    tempreps.push({ name, description: repo.description.trim(), topics });
  }
  reps = tempreps;
  console.log(reps);
  await Deno.writeTextFile("./repos.json", JSON.stringify(reps, 0, 2));
} catch (e) {
  console.error(e);
  throw new Error("Getting repos infos failed");
}

console.log("2. Deleting all repos...");
try {
  for (const rep of reps) {
    await fetch(`${instance}/api/v1/repos/${username}/${rep.name}`, {
      method: "DELETE",
      headers: {
        Authorization: "Basic " + encode(`${username}:${password}`),
      },
    });
    console.log(`Deleted ${username}/${rep.name}`);
  }
} catch (e) {
  console.error(e);
  throw new Error("Deleting repos failed");
}

console.log("2. Creating all repos...");
try {
  for (const rep of reps) {
    await fetch(`${instance}/api/v1/user/repos`, {
      method: "POST",
      headers: {
        Authorization: "Basic " + encode(`${username}:${password}`),
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name: rep.name,
        description: rep.description,
        private: false,
      }),
    });
    console.log(`Created ${username}/${rep.name}`);

    await fetch(`${instance}/api/v1/repos/${username}/${rep.name}/topics`, {
      method: "PUT",
      headers: {
        Authorization: "Basic " + encode(`${username}:${password}`),
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        topics: rep.topics,
      }),
    });
    console.log(`Topicced ${username}/${rep.name}`);
  }
} catch (e) {
  console.error(e);
  throw new Error("Creating repos failed");
}
