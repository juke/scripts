# [scripts](https://code.juke.fr/kay/scripts)
various scripts i wrote that i felt needed to be committed for some reason

just some scripts i wrote don't expect much documentation


## License

Most all of my work is now licensed under **a modified** [CC-BY-NC-SA 4.0 AFEdit](https://code.juke.fr/kay/license/raw/branch/main/LICENSE) plus accounting for states existing in our world on top of capitalism.

This is a weird choice for code right?

Here are a few key reasons:
- my definition of "open" involves being able to share and modify, you are able to do such things, just not make money off of it, or oppress people
- my definition of "free" involves being able to share and modify, you are able to do such things, not just make money off of it, or oppressing people

So no, amongst other things, this list is not exhaustive,
- you cannot have somebody work on my tool and redistribute it to your employees
- you cannot resell copies of this because in this age distribution is not done with floppy disks and the internet is a thing
- you cannot use it to generate revenue yourself
- you cannot use it to "generate value" in a capitalistic sense
- you cannot use it in any military capacity
- you cannot use it in any law enforcement capacity
- you cannot use it in any state backed capacity
- you cannot use it in any surveillance capacity
- you cannot use it if you represent the interests of a state
- you cannot use it to oppress, spy, control in any capacity
- you cannot use it to injure, harm, kill, whether physically or psychologically

You can, however,
- change it to do whatever you please
- share it to anyone you please with attribution and under the same license
- use it as much as you please
- and probably a bunch of other cool things that are possible outside of a capitalistic, imperialistic frame of reference that permeates the tech scene

Most of the "arguments" for how "free" and "open" source licenses are done still to this day stem from archaic concepts that might not even be relevant these days and I fail to see the issue with this license not being "interoperable" with a bunch of what I deem to be "bad" licenses, as they all allow for commercial usage.

I also will not make any attempts to monetize these works and will at most ever offer the possibility to donate to me directly if you enjoy what I do.

Thank you that is all.

## Development

To clone the repository locally:

```bash
$ git clone https://code.juke.fr/kay/scripts.git
```

## Contributing

More to come later.

### Issues
Open new issues by mailing [eutychia.gitlab+scripts-issue@gmail.com](mailto:eutychia.gitlab+scripts-issue@gmail.com)

---
beep boop

